const cassandra = require('cassandra-driver');
const props = require('../resources/cassandraProperties.json');
console.log('process.env.ENV='+process.env.ENV);
const client = new cassandra.Client(props[process.env.ENV]);

module.exports = client;