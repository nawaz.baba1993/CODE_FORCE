const winston = require('winston');

const logger = new (winston.Logger)({
  transports: [
    // new (winston.transports.Console)({ json: false, timestamp: true }),
    new (winston.transports.File)({ filename:'api_debug.log', json: false })
  ],
  exceptionHandlers: [
    new (winston.transports.Console)({ json: false, timestamp: true }),
    new (winston.transports.File)({ filename:'api_exceptions.log', json: false })
  ],
  exitOnError: false
});

module.exports = logger;