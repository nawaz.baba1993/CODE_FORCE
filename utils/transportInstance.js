const nodemailer = require('nodemailer');
const smtpProps = require('../resources/smtpProperties.json');

// create reusable transporter object using the default SMTP transport
let transporter = nodemailer.createTransport(smtpProps.trasport);

module.exports = transporter;