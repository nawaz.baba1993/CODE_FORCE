
const analyticsService = require('../services/analyticsService');
const LOGGER = require('../utils/logger');

module.exports = {

    getAllData: (req, res, next) => {
        analyticsService.fetchAllData()
            .then(result => res.send(result.rows))
            .catch(err => next(err));
    },

    getDataByPromoId: (req, res, next) => {
        analyticsService.fetchDataByPromoId(req.params.promoId)
            .then(result => res.send(result.rows))
            .catch(err => next(err));
    },

    updatePromo: (req, res, next) => {
        analyticsService.fetchDataByPromoAndMail(req.params.promoId, req.params.mailId)
            .then(result => {
                if (result.rows.length > 0) {
                    let data = result.rows[0];
                    data.visited = true;
                    analyticsService.saveData(data)
                        .then(result => {
                            LOGGER.info(`Marked as visited for promo:${req.params.promoId} and email:${req.params.mailId}`);
                            res.send({
                                Message: `Marked as visited`,
                                Promotion: req.params.promoId,
                                mail: req.params.mailId
                            });
                        })
                        .catch(err => LOGGER.error(err));
                } else {
                    LOGGER.info(`No data found for promo:${req.params.promoId} and email:${req.params.mailId}`);
                    res.send({
                        Message: `No data found`,
                        Promotion: req.params.promoId,
                        mail: req.params.mailId
                    });
                }
            }).catch(err => next(err));
    },

}