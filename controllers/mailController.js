
const mailService = require('../services/mailService');
const smtpProps = require('../resources/smtpProperties.json');

module.exports = {

    sendMail: (req, res, next) => {
        let mailData = req.body;
        mailData.from = smtpProps.fromEmail;
        mailService.triggerMail(mailData).then(info => {
            res.send({ Message: `Message ${info.messageId} sent: ${info.response}` });
        }).catch(err => next(err));
    },

}