const randomString = require("randomstring");
const async = require('async');

const customerService = require('../services/customerService');
const promotionService = require('../services/promotionService');
const LOGGER = require('../utils/logger');
const mailService = require('../services/mailService');
const analyticsService = require('../services/analyticsService');
const smtpProps = require('../resources/smtpProperties.json');

module.exports = {

    getAllAppPromos: (req, res, next) => {
        promotionService.fetchAllAppPromos(req.params.appname.toUpperCase())
            .then(result => res.send(result.rows))
            .catch(err => next(err));
    },

    getPromoById: (req, res, next) => {
        promotionService.fetchPromoById(req.params.appname.toUpperCase(), req.params.promoId)
            .then(result => res.send(result.rows))
            .catch(err => next(err));
    },

    addPromotion: (req, res, next) => {
        let promoData = req.body;
        promoData.appname = req.params.appname.toUpperCase();
        promoData.promo_id = `PROMO_${randomString.generate()}`;
        promoData.created = new Date();
        promoData.modified = new Date();
        promotionService.savePromotion(promoData)
            .then(result => res.send({
                promo_id: promoData.promo_id,
                message: `Promotion created successfully!`
            }))
            .catch(err => next(err));
    },

    triggerPromotion: (req, res, next) => {
        LOGGER.info(`triggering promtion ${req.params.promoId} ....!`);
        async.series({
            promotion: callback => {
                LOGGER.info(`fetching promtion ${req.params.promoId}`);
                promotionService.fetchPromoById(req.params.appname.toUpperCase(), req.params.promoId)
                    .then(result => {
                        if (result.rows.length > 0)
                            callback(null, result.rows[0]);
                        else {
                            let err = {
                                Message: `Inavlid promotion`,
                                promotion: req.params.promoId,
                                App: req.params.appname.toUpperCase()
                            };
                            callback(err, null);
                        }
                    })
                    .catch(err => callback(err, null));
            },
            users: callback => {
                LOGGER.info(`fetching users ${req.params.appname}`);
                if (req.body === undefined || req.body.email === undefined) {
                    customerService.fetchAllAppCustomers(req.params.appname.toUpperCase())
                        .then(result => {
                            if (result.rows.length > 0) {
                                callback(null, result.rows);
                            } else {
                                let err = {
                                    Message: `No Customers to trigger promotion`,
                                    App: req.params.appname.toUpperCase()
                                };
                                callback(err, null);
                            }
                        })
                        .catch(err => callback(err, null));
                } else {
                    callback(null, [req.body]);
                }
            }
        },
            (err, result) => {
                LOGGER.info(`sending mail to users ${req.params.appname}`);
                if (!err) {
                    for (let i = 0; i < result.users.length; i++) {
                        let visitData = {
                            email: result.users[i].email,
                            promoId: result.promotion.promo_id
                        }
                        let mailContent = result.promotion.content.replace('$img_src', `http://mnzbusiness.com/codeforce_api/images/${new Buffer(result.promotion.promo_id).toString('base64')}/user/${new Buffer(result.users[i].email).toString('base64')}/img.png`);
                        mailContent = mailContent.replace('$visit_url', `http://mnzbusiness.com/promotion.html?hash=${new Buffer(JSON.stringify(visitData)).toString('base64')}`);
                        console.log(mailContent);
                        let mailData = {
                            from: smtpProps.fromEmail,
                            to: result.users[i].email,
                            subject: result.promotion.promo_title,
                            html: mailContent
                        };
                        mailService.triggerMail(mailData).then(info => {
                            LOGGER.info(`Message ${info.messageId} sent: ${info.response}`);
                            if (req.body === undefined || req.body.email === undefined) {
                                let data = {
                                    promo_id: result.promotion.promo_id,
                                    customer_mail: mailData.to,
                                    read: false,
                                    visited: false,
                                    created: new Date(),
                                    modified: new Date()
                                }
                                analyticsService.saveData(data).catch(err => LOGGER.error(err));
                            }
                        }).catch(err => LOGGER.error(err));
                    }
                    res.send({
                        Message: `Successfully triggered promotion...!`,
                        promotion: req.params.promoId,
                        App: req.params.appname.toUpperCase()

                    });
                } else { next(err); }
            });

    },

}