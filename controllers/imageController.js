
const analyticsService = require('../services/analyticsService');
// const smtpProps = require('../resources/smtpProperties.json');
const LOGGER = require('../utils/logger');
const path = require('path');

module.exports = {

    updateMailStatus: (req, res, next) => {
        let promo = new Buffer(req.params.promo, 'base64').toString('ascii');
        let mailId = new Buffer(req.params.mailId, 'base64').toString('ascii');
        LOGGER.info(`Promotion opened promo:${promo} email:${mailId}`);
        analyticsService.fetchDataByPromoAndMail(promo, mailId)
        .then(result => {
            if(result.rows.length > 0) {
                let data = result.rows[0];
                data.read = true;
                analyticsService.saveData(data)
                .then(result=> LOGGER.info(`Marked as read for promo:${promo} and email:${mailId}`))
                .catch(err=>LOGGER.error(err));
            }else {
                LOGGER.info(`No data found for promo:${promo} and email:${mailId}`);
            }
        }).catch(err=>LOGGER.error(err));
        res.setHeader('content-type', 'image/png');
        res.sendFile(path.resolve('./resources/dummy.png'));
    },

}