
const customerService = require('../services/customerService');
const LOGGER = require('../utils/logger');

module.exports = {

    getAllAppCustomers: (req, res, next) => {
        customerService.fetchAllAppCustomers(req.params.appname.toUpperCase())
            .then(result => res.send(result.rows))
            .catch(err => next(err));
    },

    addCustomer: (req, res, next) => {
        let customerData = req.body;
        customerData.appname = req.params.appname.toUpperCase();
        customerData.created = new Date();
        customerData.modified = new Date();
        customerData.verified = true;
        customerData.subcribed = true;
        customerService.saveCustomer(customerData)
            .then(result => res.send({
                customer : customerData.name,
                message : `Customer created successfully!`
            }))
            .catch(err => next(err));
    },

}