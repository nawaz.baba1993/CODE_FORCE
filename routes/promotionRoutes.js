const promotionController = require('../controllers/promotionController');
var express = require('express');
var router = express.Router({ mergeParams: true });

router.get('/', promotionController.getAllAppPromos);

router.get('/:promoId', promotionController.getPromoById);

router.post('/:promoId/trigger', promotionController.triggerPromotion);

router.post('/', promotionController.addPromotion);

module.exports = router;