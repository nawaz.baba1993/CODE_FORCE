const mailController = require('../controllers/mailController');
var express = require('express');
var router = express.Router({ mergeParams: true });

router.post('/', mailController.sendMail);

module.exports = router;