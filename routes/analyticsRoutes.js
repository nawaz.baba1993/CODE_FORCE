const analyticsController = require('../controllers/analyticsController');
var express = require('express');
var router = express.Router({ mergeParams: true });

router.get('/', analyticsController.getAllData);

router.get('/:promoId', analyticsController.getDataByPromoId);

router.get('/:promoId/email/:mailId', analyticsController.updatePromo);

module.exports = router;