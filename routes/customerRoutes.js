const customerController = require('../controllers/customerController');
var express = require('express');
var router = express.Router({ mergeParams: true });

router.get('/', customerController.getAllAppCustomers);

router.post('/', customerController.addCustomer);

module.exports = router;