const imageController = require('../controllers/imageController');
var express = require('express');
var router = express.Router({ mergeParams: true });

router.get('/:promo/user/:mailId/img.png', imageController.updateMailStatus);

module.exports = router;