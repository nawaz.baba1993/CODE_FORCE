const client = require('../utils/cassandraConnectionUtil');

const SELECT_QUERY = 'SELECT * FROM promo_analytics_data';
const SELECT_QUERY_PROMO = 'SELECT * FROM promo_analytics_data where promo_id=?';
const SELECT_QUERY_PROMO_MAIL = 'SELECT * FROM promo_analytics_data where promo_id=? and customer_mail=?';
const INSERT_QUERY = 'INSERT INTO promo_analytics_data JSON ?';

module.exports = {

    fetchAllData: () => {
        return new Promise((resolve, reject) => {
            client.execute(SELECT_QUERY, (err, result) => {
                if (err)
                    reject(err);
                else
                    resolve(result);
            });
        });
    },

    saveData: (promoData) => {
        return new Promise((resolve, reject) => {
            client.execute(INSERT_QUERY, [JSON.stringify(promoData)], (err, result) => {
                if (err)
                    reject(err);
                else
                    resolve(result);
            });
        });
    },

    fetchDataByPromoId: (promoId) => {
        return new Promise((resolve, reject) => {
            client.execute(SELECT_QUERY_PROMO, [promoId], (err, result) => {
                if (err)
                    reject(err);
                else
                    resolve(result);
            });
        });
    },

    fetchDataByPromoAndMail: (promoId, mail) => {
        return new Promise((resolve, reject) => {
            client.execute(SELECT_QUERY_PROMO_MAIL, [promoId, mail], (err, result) => {
                if (err)
                    reject(err);
                else
                    resolve(result);
            });
        });
    },

}