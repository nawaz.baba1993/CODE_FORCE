const client = require('../utils/cassandraConnectionUtil');

const INSERT_QUERY = 'INSERT INTO promotion JSON ?';
const SELECT_PROMO_BY_APP = 'SELECT appname, promo_id, promo_title, created, modified FROM promotion where appname=?'
const SELECT_PROMO_BY_ID = 'SELECT * FROM promotion where appname=? and promo_id=?'

module.exports = {

    savePromotion: (promoData) => {
        return new Promise((resolve, reject) => {
            client.execute(INSERT_QUERY, [JSON.stringify(promoData)], (err, result) => {
                if (err)
                    reject(err);
                else
                    resolve(result);
            });
        });
    },

    fetchAllAppPromos: (appname) => {
        return new Promise((resolve, reject) => {
            client.execute(SELECT_PROMO_BY_APP, [appname], (err, result) => {
                if (err)
                    reject(err);
                else
                    resolve(result);
            });
        });
    },

    fetchPromoById: (appname, promoId) => {
        return new Promise((resolve, reject) => {
            client.execute(SELECT_PROMO_BY_ID, [appname, promoId], (err, result) => {
                if (err)
                    reject(err);
                else
                    resolve(result);
            });
        });
    },

}