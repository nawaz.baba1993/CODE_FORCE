const transportInstance = require('../utils/transportInstance');
const LOGGER = require('../utils/logger');

module.exports = {

    triggerMail: (mailData) => {
        return new Promise((resolve, reject) => {
            transportInstance.sendMail(mailData, (err, result) => {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(result);
                }
            });
        });
    },
}