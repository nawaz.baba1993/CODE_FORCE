const client = require('../utils/cassandraConnectionUtil');

const INSERT_QUERY = 'INSERT INTO customer JSON ?';
const SELECT_CUSTOMERS_BY_APP = 'SELECT * FROM customer where appname=?';

module.exports = {

    saveCustomer: (customerData) => {
        return new Promise((resolve, reject) => {
            client.execute(INSERT_QUERY, [JSON.stringify(customerData)], (err, result) => {
                if (err)
                    reject(err);
                else
                    resolve(result);
            });
        });
    },

    fetchAllAppCustomers: (appname) => {
        return new Promise((resolve, reject) => {
            client.execute(SELECT_CUSTOMERS_BY_APP, [appname], (err, result) => {
                if (err)
                    reject(err);
                else
                    resolve(result);
            });
        });
    },

}